# PaperCut MF App Path
$PCAppPath = "C:\Program Files\PaperCut MF"

# Directories
$KeyToolPath = Join-Path -Path $PCAppPath -ChildPath 'runtime\jre\bin\'
$KeyStorePath = Join-path -Path $PCAppPath -ChildPath 'server\custom\'
$PCWinBinPath = Join-path -Path $PCAppPath -ChildPath 'server\bin\win'

# Get available certificates
$certsStoreRoot = "\\server\Certs$\"
$certsStore = Get-ChildItem -Path $certsStoreRoot -Filter "domainFolder*" -Directory | Sort-Object -Property LastWriteTime -Descending | Select-Object -First 1
$certsStoreFolder = $certsStore.FullName
$certsTempFolder = $(Join-Path -Path $KeyStorePath -ChildPath 'temp')

Function Write-Log($msg){
    Write-Host $msg
    $now = [string]$(Get-Date -format s)
    Add-Content -Path "C:\Scripts\Logs\Install-SSLPapercut.log" -Value $($now + " - " + $msg)
}

Try{

    # Test that we can access the certs folder found above
    Test-Path $certsStoreFolder

    # Create temp dir
    Write-Log "Creating temp certs folder"
    if(!(Test-Path $certsTempFolder)){ mkdir $certsTempFolder }

    # Copy certs to local temp dir
    Write-Log "Creating temp certs folder"
    Copy-Item -Path $certsStoreFolder -Destination $certsTempFolder -Recurse
    $localCertsFolder = Join-Path -Path $certsTempFolder -ChildPath $certsStore.Name

    # Archive old keystore
    Write-Log "Archiving old keystore"
    $archiveDate = Get-Date $(Get-ChildItem -Path $(Join-Path -Path $KeyStorePath -ChildPath 'my-ssl-keystore')).LastWriteTime -UFormat "%Y-%m-%d"
    Move-Item -Path $(Join-Path -Path $KeyStorePath -ChildPath 'my-ssl-keystore') -Destination $(Join-Path -Path $KeyStorePath -ChildPath "my-ssl-keystore-$archiveDate")

    # Create new keystore from pfx
    Write-Log "Creating new keystore"
    cd $PCWinBinPath
    .\create-ssl-keystore.exe -f -p12 $('"' + $(Join-Path -Path $localCertsFolder -ChildPath $($($certsStore.Name) + ".pfx")) + '"') -p12pass PASSWORD! -k $('"' + $(Join-Path -Path $KeyStorePath -ChildPath 'my-ssl-keystore') + '"')

    # Restart PaperCut App Server
    Write-Log "Restarting PaperCut"
    Restart-Service PCAppServer 

    Write-Log "Cleaning up temp files"
    Remove-Item -Path $localCertsFolder -Recurse -Force

    Write-Log "Done"
    
    }catch{
      
      Write-Log $("ERROR: " + $_.Exception)
      
    }
